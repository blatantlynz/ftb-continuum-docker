FROM openjdk:8-jre-alpine

WORKDIR /ftb

RUN wget https://media.forgecdn.net/files/2572/478/FTBContinuumServer_1.3.1.zip -O ftb.zip

RUN unzip ftb.zip

RUN chmod +x ./FTBInstall.sh ./ServerStart.sh

RUN ./FTBInstall.sh

VOLUME [ "/ftb/world" ]

RUN echo "eula=true" > eula.txt

ADD server.properties /ftb/server.properties

EXPOSE 25565

CMD [ "./ServerStart.sh" ]