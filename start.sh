#!/bin/sh

set -ex

docker run \
    -d \
    --name ftb \
    -v "$(pwd)"/world:/ftb/world \
    -p 25565:25565\
     ftb
