#!/bin/sh

set -ex

mkdir world || true

docker rm -f ftb || true

docker build . -t ftb
